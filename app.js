import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import dotenv from 'dotenv'

const app = express();
      app.use(cors())
      app.use(express.json())

   dotenv.config();

import router from './routers/router.js'

app.use('/posts',router)

const PORT = process.env.PORT || 3000;

mongoose
  .connect('mongodb://localhost:27017/test', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(PORT, () => {
      console.log(`http://localhost:${PORT}`);
    });
  })
  .catch(err => console.log(err));

