import express from 'express'
import mongoose from 'mongoose'

import Post from '../db/posts.js'

const router = express.Router();

router.get('/get-cookies',(req, res) => {
  console.log('Cookie: ', req.cookies)
  res.send('Get Cookie')
})

router.get('/', async (req,res) => {
	try{

		const allPost = await Post.find()
		res.json(allPost)

	} catch (error){
			console.log('error-----------', error)
	}
})
//id post getiryar
router.get('/:id', async (req,res) => {
	try{
		const {id} = req.params
		const post= await Post.findById(id)
		if(!post) return
		res.status(200).json(post)

	} catch (error){
			console.log('error-----------', error)
	}
})
//post store
router.post('/', async (callback , req , res) => {
	try{
const newPost = new Post({
   title: req.body.title,
   content: req.body.content,
   author: req.body.author,
   callback()
});
	
		const createdPost = await Post.create(newPost)
		res.status(201).json(createdPost)

	} catch (error){

			console.log('error-----------', error)
	}
})
//put method 
router.put('/:id', async (req,res) => {
	try{
				const {id} = req.params
  			const {title,content,author} = req.body

  			if(!mongoose.Types.ObjectId.isValid(id))
  				return res.status(404).send('post no')
			
  			const update = {title , content, author, _id:id}

		 await Post.findByIdAndUpdate(id,update,{new:true})
		res.json(update)

	} catch (error){
			console.log('error-----------', error)
	}
})

router.delete('/:id', async (req,res) => {
	try{
				const {id} = req.params

  			if(!mongoose.Types.ObjectId.isValid(id))
  				return res.status(404).send('post no')

		 await Post.findByIdAndRemove(id)
		res.json({message:"removed post"})

	} catch (error){
			console.log('error-----------', error)
	}
})

export default router